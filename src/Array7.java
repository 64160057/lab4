import java.util.Scanner;

public class Array7 {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int arr[] = new int[3];
        System.out.print("Please input arr[0]:");
        arr[0] = kb.nextInt();
        System.out.print("Please input arr[1]:");
        arr[1] = kb.nextInt();
        System.out.print("Please input arr[2]:");
        arr[2] = kb.nextInt();
        System.out.print("arr =");
        for(int i=0; i < arr.length; i++) {
            System.out.print(" " + arr[i]);
        }
        System.out.println();
        int sum=0;
        for (int i=0; i < arr.length; i++) {
            sum += arr[i];
        }
        System.out.println("sum = " + sum );
        
        double avg = ((double)sum) / arr.length;
        System.out.println("avg = " + avg);

        int min = arr[0];
        for (int i=0; i < arr.length; i++) {
            if (arr[i] < min) {
                min = arr[i];
            }
        }
        System.out.println("min = "+ min);

    }
}
