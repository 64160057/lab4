import java.util.Scanner;

public class Array9 {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int arr[] = new int[3];
        System.out.print("Please input arr[0]:");
        arr[0] = kb.nextInt();
        System.out.print("Please input arr[1]:");
        arr[1] = kb.nextInt();
        System.out.print("Please input arr[2]:");
        arr[2] = kb.nextInt();
        System.out.print("arr =");
        for(int i=0; i < arr.length; i++) {
            System.out.print(" " + arr[i]);
        }
        System.out.println();
        System.out.print("please input search value:");
        int searchValue = kb.nextInt();
        int index = -1;
        for (int i=0; i < arr.length; i++) {
            if(arr[i] == searchValue) {
                index = i;
                break;
            }
        }
        if(index>=0) {
            System.out.println("found at index: " + index);
        } else {
            System.out.println("not found");
        }
    }
}
