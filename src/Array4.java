import java.util.Scanner;

public class Array4 {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int arr[] = new int[3];
        System.out.print("Please input arr[0]:");
        arr[0] = kb.nextInt();
        System.out.print("Please input arr[1]:");
        arr[1] = kb.nextInt();
        System.out.print("Please input arr[2]:");
        arr[2] = kb.nextInt();
        System.out.print("arr =");
        for(int i=2; i >= 0; i--) {
            System.out.print(" " + arr[i]);
        }
    }
}
